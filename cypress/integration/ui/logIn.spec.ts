/// <reference types="cypress" />

import SideMenuComponent from '../../components/sideMenu.component';
import LoginPage from '../../pages/login.page';
import WorkspaceConfigPage from '../../pages/workspace.config.page';

describe('As a user I want to be able to ->', () => {
  // Initialing pages
  const loginPage = new LoginPage();
  const workspaceConfigPage = new WorkspaceConfigPage();
  const sideMenuComponent = new SideMenuComponent();

  // Test data
  const cy_manager_login = Cypress.env('cy_manager_login');
  const cy_manager_password = Cypress.env('cy_manager_password');
  const cy_staff_login = Cypress.env('cy_staff_login');
  const cy_staff_password = Cypress.env('cy_staff_password');
  const cy_nonstaffmanager_login = Cypress.env('cy_nonstaffmanager_login');
  const cy_nonstaffmanager_password = Cypress.env('cy_nonstaffmanager_password');

  context('Login as Manager -> ', () => {
    // TC_Login_1
    it('Login with valid username & password.', () => {
      loginPage.logInWithCorrectCredentials(cy_manager_login, cy_manager_password);
    });

    // TC_Login_2
    it('Login with invalid username & password.', () => {
      loginPage.logInWithWrongCredentials(cy_manager_login, 'invalidpassword');
    });

    // TC_Login_3, TC_Login_16
    it('Login with more than 1 security role.', () => {
      loginPage.logInWithCorrectCredentials(cy_manager_login, cy_manager_password);
      workspaceConfigPage.selectSecurityRole('Manager');
      workspaceConfigPage.checkWorkspaceNumber(24);
      workspaceConfigPage.selectSecurityRole('Staff');
      workspaceConfigPage.checkWorkspaceNumber(1);
      // TODO Add assertion for Template
      // TODO Check if the user is able to login in using Manager Role
      // TODO Check if the user is able to login in using Staff Role
      // TODO Check the sidemenu tabs upon logging in for Manager Role
      // TODO Check the sidemenu tabs upon logging in for Staff Role
    });

    // TC_Login_4
    it('Login with only 1 security role.', () => {
      const workspace = 'Health Care 10';
      const tabs = ['STAFF PROFILE', 'SCHEDULE', 'TIME SHEET', 'CLAIMS', 'ADMINISTRATION', 'SETTINGS', 'LOGOUT'];
      loginPage.logInWithCorrectCredentials(cy_nonstaffmanager_login, cy_nonstaffmanager_password);
      workspaceConfigPage.checkRoleSelectorIsDisabled();
      workspaceConfigPage.checkWorkspaceNumber(24);
      workspaceConfigPage.selectWorkspace(workspace);
      workspaceConfigPage.clickContinueButton();
      sideMenuComponent.checkMenuOptions(tabs);
    });
  });

  context('Login as Staff/Employee -> ', () => {
    // TC_Login_5
    it('Login with valid username & password.', () => {
      loginPage.loginAsStaffUser(cy_staff_login, cy_staff_password);
    });

    // TC_Login_6,
    it('Login with invalid username & password.', () => {
      loginPage.logInWithWrongCredentials(cy_staff_login, 'invalidpassword');
    });
  });

  context('Verify Tabs/Modules ->', () => {
    // TC_Login_7
    it('Verify if all Tabs/Modules are present.', () => {
      const tabs = [
        'SCHEDULE',
        'REQUESTS',
        'TIME CARD',
        'MESSAGES',
        'ENTERPRISE',
        'PROFILE',
        'CHANGE PASSWORD',
        'SETTINGS',
        // Logout tab is not mentioned as it has another selector. Need to change that selector and add Logout tab to the tabs list. 
      ];
      loginPage.loginAsStaffUser(cy_staff_login, cy_staff_password);
      sideMenuComponent.checkMenuOptionsForStaff(tabs);
    });
  });

  context('UI Checking -> ', () => {
    // TC_Login_8
    it('Verify if Workspace, Group and Template Tab is displayed upon login.', () => {});

    // TC_Login_9
    it('Save Selection as Default (checkbox).', () => {
      loginPage.logInWithCorrectCredentials(cy_manager_login, cy_manager_password);
      workspaceConfigPage.checkSaveAsDefaultOption();
    });
  });

  context('Workspace Selection -> ', () => {
    // TC_Login_10
    it('Select a Workspace.', () => {});

    // TC_Login_11
    it('Select a Template.', () => {
      const templates = ['Default Workspace', 'CIA Template'];
      loginPage.logInWithCorrectCredentials(cy_manager_login, cy_manager_password);
      workspaceConfigPage.checkTemplateTabIsVisible();
      workspaceConfigPage.switchToTemplateTab();
      workspaceConfigPage.checkTemplateNumber(3);
      workspaceConfigPage.selectTemplates(templates);
      workspaceConfigPage.clickContinueButton();
      sideMenuComponent.checkMenuIconsForV6Manager();
    });

    // TC_Login_12
    it('Select a Group.', () => {});
  });

  context('Display Security Roles -> ', () => {
    // TC_Login_13
    it('User has more than 1 security role.', () => {});

    // TC_Login_14
    it('User has more than 2 security roles.', () => {
      loginPage.logInWithCorrectCredentials(cy_manager_login, cy_manager_password);
      workspaceConfigPage.selectSecurityRole('Manager');
      workspaceConfigPage.checkWorkspaceNumber(24);
      workspaceConfigPage.switchToGroupTab();
      workspaceConfigPage.checkGroupsNumber(3);
      workspaceConfigPage.switchToTemplateTab();
      workspaceConfigPage.checkTemplateNumber(3);
      workspaceConfigPage.selectSecurityRole('Staff');
      workspaceConfigPage.checkWorkspaceNumber(0);
    });

    // TC_Login_15
    it('After the security role is selected, the Workspace/Group/Template options list must be refreshed to coincide with the updates from the security role.', () => {});
  });

  context('Link to existing web employee/staff portal -> ', () => {
    // TC_Login_17
    it('If user selected a security role that only has permissions starting with web_manager_*.', () => {
      const workspace = 'Health Care 10';
      const tabs = ['STAFF PROFILE', 'SCHEDULE', 'TIME SHEET', 'CLAIMS', 'ADMINISTRATION', 'SETTINGS', 'LOGOUT'];
      loginPage.logInWithCorrectCredentials(cy_manager_login, cy_manager_password);
      workspaceConfigPage.selectSecurityRole('Manager');
      workspaceConfigPage.checkWorkspaceNumber(24);
      workspaceConfigPage.selectWorkspace(workspace);
      workspaceConfigPage.clickContinueButton();
      sideMenuComponent.checkMenuOptions(tabs);
      sideMenuComponent.checkMenuIconsForV6Manager();
    });

    // TC_Login_18
    it('If user selected a security role that only has permissions starting with desktop_manager_*.', () => {});

    // TC_Login_19
    it('If user selected a security role that only has permissions starting with web_employee_*.', () => {});

    // TC_Login_20
    it('If user has a combination of any permissions from web_manager_*, web_employee_* or desktop_manager_*.', () => {});

    // TC_Login_21
    it('If the user selected Default Workspace or any template workspace under the Template group, redirect to desktop manager.', () => {
      const tempalte = ['Default Workspace'];
      loginPage.logInWithCorrectCredentials(cy_manager_login, cy_manager_password);
      workspaceConfigPage.switchToTemplateTab();
      workspaceConfigPage.selectTemplates(tempalte);
      workspaceConfigPage.clickContinueButton();
    });

    // TC_Login_22
    it('If user selected Default workspace, he cannot selected another workspace.', () => {});

    // TC_Login_23
    it('If user selected a template workspace, he cannot select another template workspace.', () => {});

    // TC_Login_24
    it('Place Default Workspace under Templates Tab (instead of workspaces Tab).', () => {});
  });
});
