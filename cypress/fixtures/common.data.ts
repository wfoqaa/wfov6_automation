export type CommonData = {
  [key: string]: any;
};

export const commonData: CommonData = {
  menu: {
    staffProfile: 'STAFF PROFILE',
    schedule: 'SCHEDULE',
    timeSheet: 'TIME SHEET',
    claims: 'CLAIMS',
    administration: 'ADMINISTRATION',
    settings: 'SETTINGS',
    logout: 'LOGOUT',
  },
};
