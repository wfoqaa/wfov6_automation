import 'cypress-file-upload';
import 'cypress-commands';
import 'cypress-iframe';
import 'cypress-plugin-tab';
import 'cypress-wait-until';
import '@4tw/cypress-drag-drop';
import 'cypress-xpath';
import 'cypress-downloadfile/lib/downloadFileCommand';

Cypress.Commands.add('getOauthAccessToken', () => {
  Cypress.log({
    message: 'Requests token and sets in local storage',
    displayName: 'GetToken',
  });
  cy.request({
    method: 'POST',
    url: 'api/security/oauth/token',
    auth: {
      username: 'client-id',
      password: 'client-secret',
    },
    body: {
      username: Cypress.env('cy_manager_login'),
      password: Cypress.env('cy_manager_password'),
      grant_type: 'password',
    },
  }).then((response) => {
    window.localStorage.setItem('jwt', JSON.stringify(response.body));
  });
});
