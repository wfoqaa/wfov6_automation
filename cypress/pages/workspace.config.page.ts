/// <reference types="cypress" />

import SideMenuComponent from '../components/sideMenu.component';
import BasePage from './base.page';

class WorkspaceConfigPage extends BasePage {
  constructor() {
    super();
    this.url = 'manager/#/user-context-config';
    this.elements = {
      roleSelector: 'div[class="ant-select-selector"]',
      searchWorkspace: 'input[placeholder="Search workspace"]',
      workspaceSelector: '#rc-tabs-0-panel-workspace > div > div > label > span:nth-child(2)',
      checkboxSaveAsDefault: ".//span[contains(text(), 'Save as default')]",
      warningText: 'span.ant-typography',
      roleDropdown: '.ant-select-selection-item',
      role: 'div[title="name"] div',
      groupTab: '#rc-tabs-0-tab-group',
      groupSelector: '#rc-tabs-0-panel-group > div > div > label > span:nth-child(2)',
      templateTab: '#rc-tabs-0-tab-template',
      templateSelector: '#rc-tabs-0-panel-template > div > div > label > span:nth-of-type(2)',
      templateName: '#rc-tabs-0-panel-template>div',
      continue: 'button[type="button"]',
    };
    this.components = {
      sideMenuComponent: new SideMenuComponent(),
    };
  }

  // Workspace tab
  public testSearchField(name) {
    cy.get(this.elements.searchWorkspace)
      .should('have.attr', 'placeholder', 'Search workspace')
      .then(($searchField) => {
        cy.wrap($searchField).type(name).should('have.value', name);
      });
  }

  public selectWorkspace(name) {
    cy.get(this.elements.workspaceSelector).contains(name).click();
    cy.get(this.elements.workspaceSelector).siblings('span').should('have.class', 'ant-checkbox-checked');
  }

  public checkSaveAsDefaultOption() {
    cy.xpath(this.elements.checkboxSaveAsDefault)
      .siblings('span')
      .click()
      .then(($checkbox) => {
        cy.wrap($checkbox).should('have.class', 'ant-checkbox-checked');
      });
    cy.get(this.elements.warningText).should('be.visible');
  }

  public selectSecurityRole(role) {
    cy.get(this.elements.roleDropdown).click();
    cy.get(this.elements.role.replace('name', role)).click();
  }

  public checkWorkspaceNumber(number) {
    cy.get(this.elements.workspaceSelector).should(($p) => {
      expect($p).to.have.length(number);
    });
  }

  // Group tab
  public switchToGroupTab() {
    cy.get(this.elements.groupTab).click();
  }

  public checkGroupsNumber(number) {
    cy.get(this.elements.groupSelector).should(($p) => {
      expect($p).to.have.length(number);
    });
  }

  // Template tab
  public switchToTemplateTab() {
    cy.get(this.elements.templateTab).click();
  }

  public checkTemplateNumber(number) {
    cy.get(this.elements.templateSelector).should(($p) => {
      expect($p).to.have.length(number);
    });
  }

  public checkTemplateTabIsVisible() {
    cy.get(this.elements.templateTab).contains('Template', { matchCase: false }).should('be.visible').click();
  }

  public selectTemplates(templates) {
    cy.get(this.elements.templateSelector).each(($el, index, $list) => {
      if ($list[index].innerText === templates[index]) {
        $list[index].click();
      }
    });
  }

  // Rest
  public checkRoleSelectorIsDisabled() {
    cy.get(this.elements.roleSelector).parent().should('have.class', 'ant-select-disabled');
  }

  public clickContinueButton() {
    cy.get(this.elements.continue).contains('Continue').should('be.visible').click();
  }
}

export default WorkspaceConfigPage;
