/// <reference types="cypress" />

import BasePage from './base.page';
import WorkspaceConfigPage from './workspace.config.page';

class LoginPage extends BasePage {
  constructor() {
    super();
    this.url = '/#/user/login';
    this.elements = {
      login: '#username',
      password: '#password',
      submit: 'button[type="submit"]',
      errorMessage: 'div.ant-alert',
    };
    this.components = {
      workspaceConfigPage: new WorkspaceConfigPage(),
    };
  }

  loginType(login, password) {
    cy.visit(this.url, { timeout: 20000 });
    cy.get(this.elements.login).type(login, { log: false });
    cy.get(this.elements.password).type(password, { log: false });
    cy.get(this.elements.submit).click();
  }

  public logInWithCorrectCredentials(login, password) {
    this.loginType(login, password);
    cy.url().should('include', this.components.workspaceConfigPage.url);
    cy.get(this.components.workspaceConfigPage.elements.roleSelector).should('be.visible');
  }

  public logInWithWrongCredentials(login, password) {
    this.loginType(login, password);
    cy.get(this.elements.errorMessage).should('be.visible').and('have.text', 'Invalid username or password');
  }

  public loginAsStaffUser(login, password) {
    this.loginType(login, password);
    cy.url().should('include', '#/schedule');
  }
}

export default LoginPage;
