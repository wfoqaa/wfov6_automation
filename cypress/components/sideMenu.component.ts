/// <reference types="cypress" />

/* List of Tabs From V5/V6 Manager and Staff:  
["STAFF PROFILE","SCHEDULE","TIME SHEET","CLAIMS","TIME CARD","REQUEST SHEET","REQUEST","REQUESTS","ENTERPRISE","PROFILE",
"CHANGE PASSWORD","MESSAGES","ANALYTICS","PLANNING","CONSOLE","PATIENT CONSOLE","DESKTOP",'ADMINISTRATION', 'SETTINGS', 'LOGOUT']; 
*/

import BaseComponent from './base.component';

class SideMenuComponent extends BaseComponent {
  constructor() {
    super();
    this.elements = {
      navigationList: 'ul[class*="ant-menu-root"]',
      navigationTitles: 'ul[class*="ant-menu-root"]>div',
      navigationTitleStaff: '[id*="menuitem-/"]',
    };
  }

  public goToStaffProfilePage() {
    cy.get(this.elements.navigationList).contains(this.commonData.menu.staffProfile).click();
  }

  public goToSchedulePage() {
    cy.get(this.elements.navigationList).contains(this.commonData.menu.schedule).click();
  }

  public goToTimeSheetPage() {
    cy.get(this.elements.navigationList).contains(this.commonData.menu.timeSheet).click();
  }

  public goToClaimsPage() {
    cy.get(this.elements.navigationList).contains(this.commonData.menu.claims).click();
  }

  public goToAdministrationPage() {
    cy.get(this.elements.navigationList).contains(this.commonData.menu.administration).click();
  }

  public goToSettingsPage() {
    cy.get(this.elements.navigationList).contains(this.commonData.menu.settings).click();
  }

  public checkMenuOptions(tabs: string[]) {
    cy.get(this.elements.navigationTitles).each(($el, index, $list) => {
      expect($list.length).to.be.equal(tabs.length);
      expect($list[index].innerText).to.be.equal(tabs[index]);
    });
  }

  public checkMenuOptionsForStaff(tabs: string[]) {
    cy.get(this.elements.navigationTitleStaff).each(($el, index, $list) => {
      expect($list.length).to.be.equal(tabs.length);
      expect($list[index].innerText).to.be.equal(tabs[index]);
    });
  }

  public checkMenuIconsForV6Manager() {
    let n = 2;
    cy.get(this.elements.navigationTitles).each(($el, index, $list) => {
      const menuLabel = $list[index].innerText;
      const menuTabs = ['STAFF PROFILE', 'SCHEDULE', 'TIME SHEET', 'CLAIMS', 'ADMINISTRATION', 'SETTINGS', 'LOGOUT'];
      const menuTabsSVG = [
        'ACCOUNT_CIRCLE',
        'DATE_RANGE',
        'SCHEDULE',
        'RECEIPT_LONG',
        'ADMINISTRATION',
        'SETTINGS',
        'LOGOUT',
      ];
      const i = menuTabs.indexOf(menuLabel);
      const y = menuTabsSVG[i];

      cy.get(`#root > div > aside > div > div.sc-jSFjdj.jcTaHb > div > ul > div:nth-child(${n}) > li`)
        .find('span.ant-menu-title-content>span')
        .contains(menuLabel);
      cy.get(`#root > div > aside > div > div.sc-jSFjdj.jcTaHb > div > ul > div:nth-child(${n}) > li`)
        .find('svg.sc-pNWdM.XJxhY.ant-menu-item-icon')
        .should('have.attr', 'data-testid', `${y}`);
      ++n;

      cy.log(`Index of ${menuLabel} is ${i} and its equivalent SVG id is ${y}`);
    });
  }

  // public checkMenuIconsForStaff() {
  //   cy.get(this.elements.navigationTitles).each(($el, index, $list) => {
  //     let menuLabel = $list[index].innerText;
  //     const menuTabs = ["SCHEDULE", "REQUEST SHEET", "REQUEST", "REQUESTS", "TIME CARD", "MESSAGES", "ENTERPRISE", "PROFILE",
  //       "CHANGE PASSWORD", 'SETTINGS', 'LOGOUT'];

  //     const menuTabsSVG = ["SCHEDULE", "REQUEST SHEET", "REQUEST", "REQUESTS", "TIME CARD", "MESSAGES", "ENTERPRISE", "PROFILE",
  //       "CHANGE PASSWORD", 'SETTINGS', 'LOGOUT'];
  //     let i = menuTabs.indexOf(menuLabel);
  //     let y = menuTabsSVG[i];
  //     cy.get(`[data-testid="${y}"]`).should('be.visible');

  //     cy.log(`Index of ${menuLabel} is ${i} and its equivalent SVG is ${y}`);
  //   });
  // }
  // TODO To be developed - Larry.
}

export default SideMenuComponent;
